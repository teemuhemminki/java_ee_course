package com.ensode.jsf.managedbeans;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.Serializable;
import java.util.Random;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;

/**
 * NoppaBean vastaa nopanheittoon liittyvän
 * datan luomisesta, muuttamisesta ja säilyttämisestä.
 * @author Teemu Hemminki
 */
@Named(value = "noppaBean")
@SessionScoped //Pitää muistaa laittaa oikeaan scopeen. Valitsin tähän SessionScopen.
public class NoppaBean implements Serializable {

    //Annetaan attribuutit
    int tulos;
    int heitot;
    int summa;
    float keskiarvo;
    Random rand;
    
    /**
     * Luodaan uusi isntanssi ilman arvoja
     */
    public NoppaBean() {
        tulos = 0;
        heitot = 0;
        summa = 0;
        keskiarvo = 0.0f;
        rand = new Random();
    }

    /**
     * Suorittaa nopanheiton ja lisää tuloksen summaan.
     * Heittojen lukumäärä lisääntyy ja keskiarvo lasketaan uusiksi.
     */
    public void heita(){
        tulos = rand.nextInt(6) + 1;
        summa += tulos;
        heitot ++;
        keskiarvo = (float) summa / heitot;
    }
    
    /**
     * Nollaa summan, heitot ja keskiarvon.
     */
    public void nollaa(){
        //Tulosta ei tarvitse nollata, koska se on jokaisella heitolla muuttuva arvo
        summa = 0;
        heitot = 0;
        keskiarvo = 0;
    }

    public float getKeskiarvo() {
        return keskiarvo;
    }

    public void setKeskiarvo(float keskiarvo) {
        this.keskiarvo = keskiarvo;
    }
    
    public int getTulos() {
        return tulos;
    }

    public void setTulos(int tulos) {
        this.tulos = tulos;
    }

    public int getHeitot() {
        return heitot;
    }

    public void setHeitot(int heitot) {
        this.heitot = heitot;
    }

    public int getSumma() {
        return summa;
    }

    public void setSumma(int summa) {
        this.summa = summa;
    }

    public Random getRand() {
        return rand;
    }

    public void setRand(Random rand) {
        this.rand = rand;
    }
    
    
}
