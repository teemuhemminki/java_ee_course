package com.jsf.beans;

import java.io.Serializable;
import java.util.ArrayList;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

/**
 * AsiakasBean tarjoaa tietomallin asiakkaan tiedoille ja tallettaa niitä
 * listaan. Asiakkaat haetaan käyttöliittymään ja viedään tietokantaan AsiakasBeanin
 * kautta.
 * @author Teemu Hemminki
 */
@Named(value = "asiakasBean")
@SessionScoped
public class AsiakasBean implements Serializable {

    ArrayList<Asiakas> clients = new ArrayList<>();

        String nimi = "";
        String osoite = "";
        String puhelin = "";
        String email = "";
        String salasana = "";
        
    @Inject
    DbBean dbBean; //Injektoidaan DbBean jotta sitä voi käyttää ja asetetaan omaan olioonsa.

    public AsiakasBean() {
    }
        
    public void DbScan(){
        clients = dbBean.getClients(this);
    }
    
    public void AddClient(){
        dbBean.addClient(nimi, osoite, puhelin, email, salasana);
    }

    public ArrayList<Asiakas> getClients() {
        DbScan(); //Tämä ajetaan turhankin monta kertaa. Pitäisi keksiä miten tämä ajetaan vain kerran.
        return clients;
    }

    public void setClients(ArrayList<Asiakas> clients) {
        this.clients = clients;
    }

    public DbBean getDbBean() {
        return dbBean;
    }

    public void setDbBean(DbBean dbBean) {
        this.dbBean = dbBean;
    }

    public String getNimi() {
        return nimi;
    }

    public void setNimi(String nimi) {
        this.nimi = nimi;
    }

    public String getOsoite() {
        return osoite;
    }

    public void setOsoite(String osoite) {
        this.osoite = osoite;
    }

    public String getPuhelin() {
        return puhelin;
    }

    public void setPuhelin(String puhelin) {
        this.puhelin = puhelin;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSalasana() {
        return salasana;
    }

    public void setSalasana(String salasana) {
        this.salasana = salasana;
    }

    //Luodaan nested class asiakas, jotta AsiakasBean voi tarjota tietomallin
    public class Asiakas {

        String nimi = "";
        String osoite = "";
        String puhelin = "";
        String email = "";
        String salasana = "";

        public Asiakas() {
        }

        public String getNimi() {
            return nimi;
        }

        public void setNimi(String nimi) {
            this.nimi = nimi;
        }

        public String getOsoite() {
            return osoite;
        }

        public void setOsoite(String osoite) {
            this.osoite = osoite;
        }

        public String getPuhelin() {
            return puhelin;
        }

        public void setPuhelin(String puhelin) {
            this.puhelin = puhelin;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getSalasana() {
            return salasana;
        }

        public void setSalasana(String salasana) {
            this.salasana = salasana;
        }

    }       
}
