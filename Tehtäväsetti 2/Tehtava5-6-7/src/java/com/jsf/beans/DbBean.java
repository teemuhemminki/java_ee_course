package com.jsf.beans;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.inject.Named;
import javax.enterprise.context.Dependent;

/**
 * DbBean huolehtii tietokantayhteyden muodostamisesta. Tietokanta toiminnot
 * haetaan tämän pavun kautta.
 *
 * @author Teemu Hemminki
 */
@Named(value = "dbBean")
@Dependent //Eli on käytössä vain kun toinen bean tarvitsee tätä
public class DbBean implements Serializable {

    public DbBean() {
    }

    public Connection Connect() {
        Connection conn;

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException cnfe) {
            System.out.println(cnfe);
        }

        //url ja kirjautumistiedot olisi parempi hakea kokonaan connection poolin kautta.
        String url = "jdbc:mysql://localhost:3306/javaee";

        try {
            conn = (Connection) DriverManager.getConnection(url, "admin", "1234");

            return conn;
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
    }

    public ArrayList<AsiakasBean.Asiakas> getClients(AsiakasBean ab) {

        ArrayList<AsiakasBean.Asiakas> CustomerList = new ArrayList<>();

        try {
            Connection conn = Connect();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM asiakkaat");

            while (rs.next()) {
                //luetaan tietueen tiedot
                String nimi = rs.getString("nimi");
                String osoite = rs.getString("osoite");
                String puhelin = rs.getString("puhelin");
                String email = rs.getString("email");
                String salasana = rs.getString("salasana");

                // Luodaan papu johon asiakkaan tiedot sijoitetaan
                AsiakasBean.Asiakas bean = ab.new Asiakas();
                //tiedot papuun
                bean.setNimi(nimi);
                bean.setOsoite(osoite);
                bean.setPuhelin(puhelin);
                bean.setEmail(email);
                bean.setSalasana(salasana);

                //lisätään jokainen asiakas papuna ArrayListiin
                CustomerList.add(bean);
            }
            conn.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return CustomerList;
    }

    public void addClient(String nimi, String osoite, String puhelin, String email, String salasana) {
        String query
                = "INSERT INTO asiakkaat (nimi, osoite, puhelin, email, salasana) VALUES ('"
                + nimi + "', '" + osoite + "', '" + puhelin
                + "', '" + email + "', '" + salasana + "');";
        try {
            Connection conn = Connect();
            Statement stmt = conn.createStatement();
            stmt.executeUpdate(query);
            conn.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}
