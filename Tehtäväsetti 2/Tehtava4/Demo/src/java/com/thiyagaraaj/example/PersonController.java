
package com.thiyagaraaj.example;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * PersonController tallettaa ja esittää person olioiden tietoja listassa.
 * @author Teemu Hemminki
 */
@Named(value = "personController")
@SessionScoped
public class PersonController implements Serializable {

    Person person;
    ArrayList<Person> personList = new ArrayList<>();

    /**
     * Luo uuden PersonControllerin ja asetttaa sine demo tietueet
     */
    public PersonController() {
        person = new Person();
        //Luodaan perus tieteet demo käyttöön
        personList.add(new Person("Google", "1", "Search Engine"));
        personList.add(new Person("Windows","2","Operating System"));
        personList.add(new Person("Oracle","3","Database"));
        personList.add(new Person("Facebook","4","Social Networking"));        
    }

    public ArrayList<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(ArrayList<Person> personList) {
        this.personList = personList;
    }
    
    /**
     * Lisää tämnhetisen person olion listaan ja luo uuden tyhjän person olion
     * käytettäväksi. Personin tiedot on asetettu formin käyttämillä
     * settereillä.
     * @return 
     */
    public String addPerson(){
        personList.add(person);        
        person = new Person();
        return "Success";
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
    
}
