package com.thiyagaraaj.example;

/**
 * Talletusolio formista saaduille henkilötiedoille.
 * @author Teemu Hemminki
 */
public class Person {
    
    String name;
    String no;
    String address;
    
    public Person(){
    }
    
    /**
     * Perus Person tietojen käyttämä konstruktori
     * @param name
     * @param no
     * @param address 
     */
    public Person(String name, String no, String address){
        this.name = name;
        this.no = no;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    
}
