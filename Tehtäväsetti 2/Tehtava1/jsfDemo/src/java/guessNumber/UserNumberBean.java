/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guessNumber;

import java.io.Serializable;
import java.util.Random;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 * UserNumberBean arpoo satunnaisen luvun 1 ja 10 väliltä
 * Huom: ManagedBeanit alkavat deprecoitua ja korvautua CDI beaneilla.
 * @author user
 */
@ManagedBean(name="UserNumberBean")
@SessionScoped
public class UserNumberBean implements Serializable {

    Integer randomInt;
    Integer userNumber;
    String response;

    //UserNumberBean instantioidaan samalla kun ohjelma käynnistyy
    public UserNumberBean() {
        Random randomGR = new Random();
        randomInt = randomGR.nextInt(10);
        System.out.println("Duke's number: " + randomInt);
    }

    public Integer getUserNumber() {
        return userNumber;
    }

    //index.html:ässä setUserNumber ajetaan ja userNumberille asetetaan
    //arvo submit nappia painettaessa.
    public void setUserNumber(Integer userNumber) {
        this.userNumber = userNumber;
    }

    public String getResponse() {
        //Tarkastetaan onko syötetty numero sama kuin arvottu numero
        if((userNumber != null) && (userNumber.compareTo(randomInt) == 0)){
            //Invalidoitaan käyttäjän sessio, jotta käyttäjä voisi arvata uuden numeron.
            FacesContext context = FacesContext.getCurrentInstance();
            HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
            session.invalidate();
            
            return "Yay! You got it!"; //Vastaus jos arvattiin oikein
        } else {
            //Muutoin pyydetään arvaamaan uudestaan
            return "<p>Sorry, " + userNumber + " isn't it.</p>"
                    + "<p>Guess again...</p>";
        }
    }

}
