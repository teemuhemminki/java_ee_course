package com.ensode.jsf.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("ageValidator")

public class AgeValidator implements Validator {

    @Override
    public void validate(FacesContext facesContext, UIComponent uIComponent, Object value)
            throws ValidatorException {

        HtmlInputText htmlInputText
                = (HtmlInputText) uIComponent;

        int age = (int) value;

        int maxAge = 120;
        int minAge = 1;

        String label;

        if (htmlInputText.getLabel() == null
                || htmlInputText.getLabel().trim().equals("")) {
            label = htmlInputText.getId();
        } else {
            label = htmlInputText.getLabel();
        }

        if (age > maxAge || age < minAge) {
            FacesMessage facesMessage
                    = new FacesMessage(label
                            + ": age must be within " + minAge + " and " + maxAge);
            throw new ValidatorException(facesMessage);
        }
    }
}
