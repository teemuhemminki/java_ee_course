<%-- 
    Document   : response
    Created on : 16.11.2017, 19:06:43
    Author     : user

    response.jsp vastaanottaa formista tulleet tiedot ja tallettaa ne infobean
    olioon. Tämän jälkeen response.jsp näyttää annetut tiedot.
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:useBean id="infobean" scope="session" class="com.mypackage.form.InfoHandler" />
        <jsp:setProperty name="infobean" property="*"/>
        
        <table>
            <tr>
                <th>Name</th>
                <th>Address</th>
                <th>City</th>
                <th>Email</th>
            </tr>
            <tr>
                <td><jsp:getProperty name="infobean" property="name"/></td>
                <td><jsp:getProperty name="infobean" property="address"/></td>
                 <td><jsp:getProperty name="infobean" property="city"/></td>
                <td><jsp:getProperty name="infobean" property="email"/></td>
            </tr>
            
            <a href="edit.jsp">Linkki</a>
    </body>
</html>
