<%-- 
    Document   : response
    Created on : 16.11.2017, 19:06:43
    Author     : user

    edit.jsp hakee beaniin talletetut tiedot lomakkeelle, josta ne voidaan
    uudelleen muokata ja lähettää.

--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%-- Bean joka on talletettu session scopeen, haetaan tässä käyttöön.
        Kyseessä on sama bean joka alunperin luotiin response.jsp tiedostossa. --%>
        <jsp:useBean id="infobean" scope="session" class="com.mypackage.form.InfoHandler" />
        <h1>Edit information</h1>
        <jsp:getProperty name="infobean" property="name"/>
        <form action="response.jsp">
            Name
            <input type="text" name="name" value=<jsp:getProperty name="infobean" property="name"/> />
            Address
            <input type="text" name="address" value=<jsp:getProperty name="infobean" property="address"/> />
            City
            <input type="text" name="city" value=<jsp:getProperty name="infobean" property="city"/> />
            Email
            <input type="text" name="email" value=<jsp:getProperty name="infobean" property="email"/> />
            <input type="submit" value="Edit" />
        </form>
    </body>
</html>
