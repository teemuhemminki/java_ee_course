<%-- 
    Document   : index
    Created on : 16.11.2017, 19:04:48
    Author     : user

    index.jsp pyytää annettavia tietoja ja lähettää ne response.jsp sivulle.
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Add information</h1>
        <form action="response.jsp">
            Name
            <input type="text" name="name" value="" />
            Address
            <input type="text" name="address" value="" />
            City
            <input type="text" name="city" value="" />
            Email
            <input type="text" name="email" value="" />
            <input type="submit" value="Send" />
        </form>
    </body>
</html>
