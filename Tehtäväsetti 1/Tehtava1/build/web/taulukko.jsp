<%-- 
    Document   : taulukko
    Created on : 6.11.2017, 16:13:46
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table>
        <% //Avataan jsp scriplet tagi

            /*
            Haetaan request.getParameter metodilla raja arvoa. Tämä muokataan
            sitten Integer arvoksi. Integer.parseInt metodilla.
            
            Try catchilla varmistetaan että raja arvo oli käypä. Jos ei ollut,
            ilmoitetaan että ei saatu kokonaislukua.
            */
            int raja;
            
            try {
                raja = Integer.parseInt(request.getParameter("raja"));
            } catch (Exception e){
                raja = 0;
                %> Ei annettua kokonaisluku arvoa! <%
            }

            /*
            Toteutetaan for silmukka, jonka väleissä jsp scriplet tageilla
            tuodaan html koodia tarpeen mukaan. Huomaa että numeroarvo tuodaan
            %= tagilla.
            */            
            for(int i = 0; i < raja; i++){
                %><tr><td>
                <%= i + 1 %>
                </td></tr><%
            }
        %>
        </table>
    </body>
</html>
