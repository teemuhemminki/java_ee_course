<jsp:useBean id="papu" class="classes.CustomerBean"
             scope="session">
</jsp:useBean>
<html>
    <head>
        <title>
            muokkaus.jsp
        </title>
    </head>
    <body>
        <p>
            Muokkaa tietueita:
        </p>
        Nimi: <jsp:getProperty name="papu" property="nimi"/>
        <br />
        Osoite: <jsp:getProperty name="papu" property="osoite"/>
        <br />
        Puhelin: <jsp:getProperty name="papu" property="puhelin"/>
        <br />
        Email: <jsp:getProperty name="papu" property="email"/>
        <br />
        Salasana: <jsp:getProperty name="papu" property="salasana"/>
        <br />
        <p>
            <a href="muokkaus.jsp">Muokkaa tietuetta</a>
            <a href="lomake.html">Palaa hakulomakkeeseen</a>
        </p>
    </body>
</html>
