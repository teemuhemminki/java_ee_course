package classes;

/*
 * SQL.java
 * Yksinkertainen luokka tietokannan käsittelyyn
 * attribuutit ja metodit staattisia eli tätä kutsutaan luokan nimellä.
 * Ei tarvitse luoda oliota.
 * 
 * Yhteys otetaan Datasourceen jonka takana on Connection Pool
 *
 *
 * Kyselyjen suoritusmetodit voi muuttaa spesifisiksi sovellukselle.
 * Nyt ne ovat kaikille sovelluksille yleiskäyttöisiä, eivätkä siksi
 * tuo paljon lisäarvoa verrattuna kyselyn suorittamiseen suoraan 
 * valmiilla Java-API:n metodeilla.
 *
 *
 */
import java.sql.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class SQL {

    private static Connection conn = null;

    // avaa yhteyden tietokantaan
    public static Connection openConnection() {

        conn = null;
/*
        try {
            /*Seuraavilla kolmella lauseella otetaan yhteys Connection Pooliin
             * JDNI-nimi on "jdbc/sample" eli se viittaa jdbc/sample-
             * resurssiin jossa on viittaus connection pooliin. 
             */
            /*
            Context ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("jdbc/sample");
            conn = ds.getConnection();

        } catch (Exception ex) {
        }

        return conn;
        */
        
         //jos halutaan ottaa yhteys jdbc:llä ilman connection poolia
         try {
         Class.forName("com.mysql.jdbc.Driver");
            
        
         } catch (ClassNotFoundException cnfe) {
         System.out.println(cnfe);
         }
         conn = null;
        
        
         String url = "jdbc:mysql://localhost:3306/javaee";
        
         try {
           
         conn = DriverManager.getConnection(url, "root", "");
          
         } catch(SQLException e){
         System.out.println(e);
         }
       
	   
         return conn;
	
         
    }

    // sulkee yhteyden
    public static void closeConnection(Connection conn) throws Exception {
        conn.close();
    }

    // luo yhteydesta Statement-olion
    public static Statement createStmt(Connection conn) {
        try {
            return conn.createStatement();
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    // luo yhteydesta PreparedStatement olion tietyn SQL-lauseen mukaisesti
    public static PreparedStatement createPreStmt(Connection conn, String sql) {
        try {
            return conn.prepareStatement(sql);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    // hakee tietokannasta tietoa Statementin ja SQL-lauseen avulla
    public static ResultSet getRS(Statement stmt, String query) {
        try {
            return stmt.executeQuery(query);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    // tekee paivityksen tietokantaan Statementin ja SQL-lauseen avulla
    public static void updateDB(Statement stmt, String query) {
        try {
            stmt.executeUpdate(query);
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
}
