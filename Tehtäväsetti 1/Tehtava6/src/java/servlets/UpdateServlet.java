/*
 * UpdateServlet toimii samalla periaatteella kuin DispatcherServlet,
 * mutta päivittääkin lomakkeen antamat tietueet papuun ja tietokantaan.
 */

package servlets;

import classes.CustomerBean;
import classes.SQL;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "UpdateServlet", urlPatterns = {"/UpdateServlet"})
public class UpdateServlet extends HttpServlet {
    
   Connection conn = null;

   String nimi = "";
   String osoite = "";
   String puhelin = "";
   String email = "";
   String salasana = "";

    /*init-metodi suoritetaan aina kun Servletti otetaan käyttöön.
     Se soveltuu hyvin tietokantayhteyden avaamiseen*/
    @Override
    public void init(ServletConfig config) throws ServletException {
        //Kutsutaan perittävän luokan konstruktoria.
        super.init(config);
       try {
       conn = SQL.openConnection();
        } catch (Exception e){
           System.out.println("Poikkeus " + e);
       }
}
    //destroy-metodi soveltuu hyvin tietokantayhteyden sulkemiseen.
    @Override
    public void destroy(){

         try {
          conn.close();
        } catch ( SQLException se ) {
           System.out.println("Poikkeus " + se);
        }
   }

   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       
    PrintWriter out = response.getWriter();

     // Otetaan istunto haltuun
    HttpSession istunto = request.getSession(false);

    // Otetaan papu sessiosta käyttöön
    CustomerBean papu = (CustomerBean) istunto.getAttribute("papu");
    
    // Tuodaan lomakkeen uudistetut tiedot käyttöön
    nimi = request.getParameter("nimi");
    osoite = request.getParameter("osoite");
    puhelin = request.getParameter("puhelin");
    email = request.getParameter("email");
    salasana = request.getParameter("salasana");

     try {
    //luodaan Statement-olio jonka avulla voidaan suorittaa sql-lause
   Statement stmt = conn.createStatement();

   //Kasataan request stringi joka käsittelee requestin.
   //Tässä voisi tutkia jotain stringBuilderin käytön hyötyjä.
   String sqlRequest =
   "UPDATE asiakkaat "
   +"SET nimi = '" + nimi
   +"', osoite = '" + osoite
   +"', puhelin = '" + puhelin
   +"', email = '" + email           
   +"', salasana = '" + salasana
   +"' WHERE email = '" + email + "'";
   
   System.out.println(request);
   
   //Suoritetaan sql, syötetään uudet arvot
   stmt.executeUpdate(sqlRequest);

     } catch (Exception e) {
           out.println("Tuli poikkeus!");
    }
  
    //uudet tiedot papuun
     papu.setNimi(nimi);
     papu.setOsoite(osoite);
     papu.setPuhelin(puhelin);
     papu.setEmail(email);
     papu.setSalasana(salasana);
   
    //uudistunut papu sessioon
    istunto.setAttribute("papu", papu);
    
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }



}

