/*
 * ControllerServlet ohjailee viestejä käyttöliittymältä
 * palveluja suorittaville tiedostoille (palveluservleteille)
 *
 */

package servlets;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ControllerServlet", urlPatterns = {"/ControllerServlet"})
public class ControllerServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {

      // Luodaan istunto ellei sitä vielä ole:
    HttpSession istunto = request.getSession(true);

    //Ohjataan pyyntö palveluservletille
    //Tässä voitaisiin (pyynnön perusteella (if)) valita
    //jatkokäsittelyn tekevä servletti, jos niitä olisi useita

    //RequestDispatcher-olio tarvitaan tietojen välittämiseksi tiedostolta toiselle
    
    //Olen melko varma että vertailuun voitaisiin käyttää jotain järkevämpääkin attribuuttia.
    if(request.getParameter("Submit").equals("Laheta")){    
        RequestDispatcher rd = request.getRequestDispatcher("/DispatcherServlet");
        rd.include(request, response);
    } else if (request.getParameter("Submit").equals("Muokkaa")){
        RequestDispatcher rd = request.getRequestDispatcher("/UpdateServlet");
        rd.include(request, response);
    }

    //Kontrolli palaa tähän kun DispatcherServlet on tehnyt työnsä.

    //Valitaan kutsuttava JSP-sivu. Jos vaihtoehtoja olisi useita, voitaisiin valita
    //iffityksellä.

   //siirrytään vastaus.jsp-sivulle
    request.getRequestDispatcher("vastaus.jsp").forward(request,response);

    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }


}



 