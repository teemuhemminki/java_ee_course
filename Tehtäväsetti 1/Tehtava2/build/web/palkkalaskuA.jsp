<%-- 
    Document   : taulukko
    Created on : 6.11.2017, 16:13:46
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%  /*
            Luodaan muuttujat ja haetaan niiden arvot. Varmistetaan että
            saadut arvot ovat valideja try catch metodilla.
            */
            float tunnit;
            float palkka;
            int verot;
            
            float brutto;            
            float netto;
            
            try {
                tunnit = Float.valueOf(request.getParameter("tunnit"));
            } catch (Exception e){
                tunnit = 0.0f;
                %> Ei annettuja tunteja <%
            }

            try {
                palkka = Float.valueOf(request.getParameter("palkka"));
            } catch (Exception e){
                palkka = 0.0f;
                %> Ei annettua palkkaa <%
            }

            try {
                verot = Integer.parseInt(request.getParameter("verot"));
            } catch (Exception e){
                verot = 0;
                %> Ei annettua veroprosenttia <%
            }

            /*
            Toteutetaan laskut saatujen arvojen pohjalta ja näytetään ne
            */
            brutto = tunnit * palkka;
            netto = brutto - (brutto * verot/100);
            %>
            <p>Bruttopalkka = <%= brutto %>€</p>
            <p>Nettopalkka = <%= netto %>€</p>
    </body>
</html>
