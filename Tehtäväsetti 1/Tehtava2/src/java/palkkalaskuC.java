
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet joka suorittaa palkan laskun ja tulostamisen
 * @author Teemu Hemminki
 */

//Annetaan tiedostolle url osoite.
@WebServlet(urlPatterns = {"/palkkalaskuC"})

//Luodaan luokka
public class palkkalaskuC extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    /*
    Täytetään doGet metodi, joka vastaa http GETin käsittelystä.
    HUOM: Tässä vaihdettu doGetistä doPostiksi, joka on tietoturva mielessä
    parempi vaihtoehto.
    */
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        
        response.setContentType("text/html");
        
        PrintWriter out = response.getWriter();
        
        String errors = "";
        
        float tunnit;
        float palkka;
        int verot;
            
        float brutto;            
        float netto;
            
        try {
            tunnit = Float.valueOf(request.getParameter("tunnit"));
        } catch (Exception e){
            tunnit = 0.0f;
            errors += "Ei annettuja tunteja. ";
        }

        try {
            palkka = Float.valueOf(request.getParameter("palkka"));
        } catch (Exception e){
            palkka = 0.0f;
            errors += "Ei annettua palkkaa. ";
        }

        try {
            verot = Integer.parseInt(request.getParameter("verot"));
        } catch (Exception e){
            verot = 0;
           errors += "Ei annettua veroprosenttia.";
        }
        
        brutto = tunnit * palkka;
        netto = brutto - (brutto * verot/100); 
        
        //Lisätään requestille attribuutit brutto ja netto.
        request.setAttribute("brutto", brutto);        
        request.setAttribute("netto", netto);
        
        /*
        Laskujen ja requestiin lisäyksen jälkeen lähetetään tiedot takaisin
        jsp tiedostolle.
        */
        RequestDispatcher rd = request.getRequestDispatcher("/palkkalomakeC.jsp");
        rd.forward(request, response);       

    }
}
