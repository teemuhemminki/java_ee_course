<%-- 
    Document   : taulukko
    Created on : 6.11.2017, 16:13:46
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <!-- Formi joka lähettää datan servletille.
        Vaihdettu tässä GETistä POSTiksi, joka on turvallisuus mielessä parempi-->
        <form action="./palkkalaskuC" method="post">
            <input type="text" name="tunnit" value="Anna tuntimäärä">
            <input type="text" name="palkka" value="Anna tuntipalkka">            
            <input type="text" name="verot" value="Anna veroprosentti">
            <input type="submit" value="Submit">
        </form>
        
        <% 
            /*
            Haetaan brutto ja nettotulo jonka servletti on palauttanut.
            Try catch antaa epäonnistuttaessa 0 arvon.
            */
            float brutto;
            float netto;
            
            try{
                brutto = (float) request.getAttribute("brutto");
            } catch (Exception e){
                brutto = 0.0f;
            }
            
            try{
                netto = (float) request.getAttribute("netto");
            } catch (Exception e){
                netto = 0.0f;
            }
        %>
        
        <p>Bruttopalkka = <%= brutto %> euroa</p>
        <p>Nettopalkka = <%= netto %> euroa</p>
    </body>
</html>
