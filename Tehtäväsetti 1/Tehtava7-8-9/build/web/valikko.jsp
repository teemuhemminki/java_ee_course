﻿<%--Sivujen yläosaan incluudattava valikko jossa tarkistetaan onko
loggauduttu sisään ja jossa voidaan loggautua ulos sovelluksesta--%>
<%
    //Katsotaan onko henkilö loggautunut
    String log = (String) session.getAttribute("login");

    if (log.equals("ok")) {
%>

<form name="kirjaudu_ulos" method="post" action="ServletLoginUlos">
    <a href='AsiakkaatServlet'>Asiakkaat</a> |
    <a href='lisaaAsiakas.jsp'>Lisää asiakas</a> |
    <a href='EmailServlet'>Poista asiakas</a> |
    <input type="submit" name="Submit" value="Kirjaudu ulos">
</form>

<%-- Haku lomake käyttää actionina HakuServlet palvelua,
ehdotetun haku.jsp tiedoston sijaan. --%>
<form name="hae_tietueita" method="post" action="HakuServlet">
Hae tietueita: 
<input type="text" name="hakusana" />
                        <select name="hakuvalinta">
                            <option value="nimi">Nimi</option>
                            <option value="osoite">Osoite</option>
                            <option value="puhelin">Puhelin</option>
                            <option value="email">Email</option>
                        </select>
<input type="submit" name="Submit" value="Hae" />
</form>

<%
    } else {
        //ei-loggautuneet takaisin login-sivulle
        response.sendRedirect("login.htm");
    }
%>

