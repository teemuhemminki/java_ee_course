<%-- 
    Document   : muokkaaAsiakas
    Author     : TuiTo
--%>

<%--
muokkaaAsiakas vastaa asiakkaan tietojen hakemisesta pavun kautta,
esittämisestä lomakkeessa ja muokkausten lähettämisestä ServletMuokkaaAsikas
servletille.
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<jsp:useBean id="papu" scope="session" class="TavallisetLuokat.CustomerBean" />

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
<form action="ServletMuokkaaAsiakas" method="post">

            <table>
                <tr>
                    <td>
                        Nimi:
                    </td>
                    <td align = "right">
                        <input type="text" name="nimi" value=<jsp:getProperty name="papu" property="nimi" /> >
                    </td>
                </tr>
                <tr>
                    <td>          
                        Osoite:
                    </td>
                    <td align = "right">
                        <input type="text" name="osoite" value=<jsp:getProperty name="papu" property="osoite" /> >
                    </td>
                </tr>
                <tr>
                    <td>                   
                        Puhelin:
                    </td>
                    <td align = "right">
                        <input type="text" name = "puhelin" value=<jsp:getProperty name="papu" property="puhelin" /> >
                    </td>
                </tr>                    
                <tr>
                    <td>                   
                        Email:
                    </td>
                    <td align = "right">
                        <input type="text" name= "email" value=<jsp:getProperty name="papu" property="email" /> >
                    </td>
                </tr> 
                <tr>
                    <td>
                        Salasana:
                    </td>
                    <td align = "right">              
                        <input type="password" name="salasana" value=<jsp:getProperty name="papu" property="salasana" /> >
                    </td>
                </tr> 
                <tr>
                    <td> 
                        <%-- Talletetaan edellinen salasana tänne.--%>
                        <input type="hidden" name="emailGet" value=<jsp:getProperty name="papu" property="email" /> readonly>
                        <input type="submit" value="Muokkaa">
                    </td>
                </tr>                    
            </table>
        </form>
    </body>
</html>
