package Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



@WebServlet(name = "ServletMuokkaaAsiakas", urlPatterns = {"/ServletMuokkaaAsiakas"})
public class ServletMuokkaaAsiakas extends HttpServlet {

    Connection conn = null;

    /**
     * Initializes the servlet.
     * @param config
     * @throws javax.servlet.ServletException
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        conn = TavallisetLuokat.SQL.openConnection();

    }

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String nimi = request.getParameter("nimi");
        String osoite = request.getParameter("osoite");
        String puhelin = request.getParameter("puhelin");
        String email = request.getParameter("email");
        String salasana = request.getParameter("salasana");
        String emailPrevious = request.getParameter("emailGet");
        

        // Varmistetaan, että käyttäjä on täyttänyt jokaisen syöttökentän
        if (nimi == null || nimi.length() == 0 || email == null || email.length() == 0
                || salasana == null || salasana.length() == 0) {
            out.println("Nimi, salasana ja sähköpostiosoite annettava!");
        } else {
            // Esitellään PreparedStatement-olio, johon asetetaan myöhemmin lisäyskomento
            PreparedStatement pre_stmt = null;

            // Kaikki tietokantatoiminnot suoritetaan try-lohkon sisällä
            try {
                // Muodostetaan tietokantakomento
                String sql = "UPDATE asiakkaat SET nimi=?, osoite=?, puhelin=?, email=?, salasana=? WHERE email='" + emailPrevious + "'";

                // Alustetaan olio luodulla merkkijonolla.
                pre_stmt = conn.prepareStatement(sql);

                // Asetetaan sql-komentoon parametrien paikalle käyttäjän syöttämät tiedot.
                pre_stmt.setString(1, nimi);
                pre_stmt.setString(2, osoite);
                pre_stmt.setString(3, puhelin);
                pre_stmt.setString(4, email);
                pre_stmt.setString(5, salasana);

                // executeUpdate-komento suorittaa sql-komennon eli tässä lisäyksen tietokantaan.
                pre_stmt.executeUpdate();

                response.sendRedirect("AsiakkaatServlet");
            } catch (SQLException e) {
                out.println("Tietojen päivitys epäonnistui, poikkeus " + e);
            } // Finally-lohkossa suljetaan PreparedStatement-olio.
            finally {
                try {
                    pre_stmt.close();
                } catch (SQLException e) {
                    System.out.println(e);
                }
            }

        }

        out.close();
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
