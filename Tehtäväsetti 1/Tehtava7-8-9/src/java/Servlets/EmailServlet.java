/*
 * EmailServlet on palveluservletti joka 
 * listaa mailiosoitteet poiston käyttöliittymää varten
 *
 */

package Servlets;


import TavallisetLuokat.SQL;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;

@WebServlet(name = "EmailServlet", urlPatterns = {"/EmailServlet"})
public class EmailServlet extends HttpServlet {

    
   Connection conn = null;

   
   String email = "";
   

    /*init-metodi suoritetaan aina kun Servletti otetaan käyttöön.
     Se soveltuu hyvin tietokantayhteyden avaamiseen*/
    @Override
    public void init(ServletConfig config) throws ServletException {
        //Kutsutaan perittävän luokan konstruktoria.
        super.init(config);
       try {
       conn = SQL.openConnection();
        } catch (Exception e){
           System.out.println("Poikkeus " + e);
       }
}
    //destroy-metodi soveltuu hyvin tietokantayhteyden sulkemiseen.
    @Override
    public void destroy(){

         try {
          conn.close();
        } catch ( SQLException se ) {
           System.out.println("Poikkeus " + se);
        }
   }

   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {

       PrintWriter out = response.getWriter();

    //lista johon mailit tulevat 
    ArrayList<String> emailList = new ArrayList<>();

     try {
 //luodaan Statement-olio jonka avulla voidaan suorittaa sql-lause
   Statement stmt = conn.createStatement();

   //Suoritetaan sql, otetaan tulosjoukko kiinni.
   ResultSet rs = stmt.executeQuery("SELECT email FROM asiakkaat");

   //Käydään tulosjoukko läpi
   while ( rs.next() )
   {
    
    email = rs.getString("email");
   
    
    emailList.add(email);
   
   }
} catch (SQLException e) {
           out.println("Tuli poikkeus!");
       }
  
         request.setAttribute("list", emailList);//lähetetään pyynnössä list

        //RequestDispatcher -oliolla voidaan lähettää dataa jsp-sivulle 
        RequestDispatcher rd = getServletConfig().getServletContext().getRequestDispatcher("/poistaAsiakas.jsp");
        rd.forward(request, response);

    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

}

