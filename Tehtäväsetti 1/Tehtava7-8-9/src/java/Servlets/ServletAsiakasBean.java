/*
 * ServletAsiakasBean hakee sähköpostin mukaisen asiakkaan,
 * tallettaa tämän tiedot papuun ja muokkaaAsiakas.jsp viewin.
 */
package Servlets;

import TavallisetLuokat.SQL;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import TavallisetLuokat.CustomerBean;

/**
 *
 * @author TuiTo
 */
@WebServlet(name = "ServletAsiakasBean", urlPatterns = {"/ServletAsiakasBean"})
public class ServletAsiakasBean extends HttpServlet {

    Connection conn = null;

    String nimi = "";
    String osoite = "";
    String puhelin = "";
    String email = "";
    String salasana = "";

    /*init-metodi suoritetaan aina kun Servletti otetaan käyttöön.
     Se soveltuu hyvin tietokantayhteyden avaamiseen*/
    @Override
    public void init(ServletConfig config) throws ServletException {
        //Kutsutaan perittävän luokan konstruktoria.
        super.init(config);
        try {
            conn = SQL.openConnection();
        } catch (Exception e) {
            System.out.println("Poikkeus " + e);
        }
    }

    //destroy-metodi soveltuu hyvin tietokantayhteyden sulkemiseen.
    @Override
    public void destroy() {

        try {
            conn.close();
        } catch (SQLException se) {
            System.out.println("Poikkeus " + se);
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        
        // Otetaan istunto haltuun
        HttpSession istunto = request.getSession(false);
        
        // Luodaan papu johon vastaus sijoitetaan
        CustomerBean papu = new CustomerBean();

        // Käsitellään lomake
        // Tehdään tietokantahaku ja tallennetaan tiedot papuun
        String em = request.getParameter("email");

        try {
            //luodaan Statement-olio jonka avulla voidaan suorittaa sql-lause
            Statement stmt = conn.createStatement();

            //Suoritetaan sql, otetaan tulosjoukko kiinni.
            ResultSet rs = stmt.executeQuery("SELECT * FROM asiakkaat WHERE email = '" + em + "'");

            //Käydään tulosjoukko läpi
            while (rs.next()) {
                //luetaan tietueen tiedot
                nimi = rs.getString("nimi");
                osoite = rs.getString("osoite");
                puhelin = rs.getString("puhelin");
                email = rs.getString("email");
                salasana = rs.getString("salasana");

            }
        } catch (Exception e) {
            out.println("Tuli poikkeus!");
        }

        //tiedot papuun
        papu.setNimi(nimi);
        papu.setOsoite(osoite);
        papu.setPuhelin(puhelin);
        papu.setEmail(email);
        papu.setSalasana(salasana);

        //papu sessioon
        istunto.setAttribute("papu", papu);

        response.sendRedirect("muokkaaAsiakas.jsp");
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
