/*
 * HakuServlet hakee asiakkaat joilla on hakuarvoa vastaava
 * attribuutti.
 * Perustuu AsiakkaatServlet luokkaan ja olisi voitu
 * periaatteessa sisällyttää kyseiseen luokkaan.
 */

package Servlets;

import TavallisetLuokat.CustomerBean;
import TavallisetLuokat.SQL;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
// import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;

@WebServlet(name = "HakuServlet", urlPatterns = {"/HakuServlet"})
public class HakuServlet extends HttpServlet {
    
   Connection conn = null;

   String nimi = "";
   String osoite = "";
   String puhelin = "";
   String email = "";
   String salasana = "";
   String hakusana = "";
   String hakuvalinta = "";
   
    /*init-metodi suoritetaan aina kun Servletti otetaan käyttöön.
     Se soveltuu hyvin tietokantayhteyden avaamiseen*/
    @Override
    public void init(ServletConfig config) throws ServletException {
        //Kutsutaan perittävän luokan konstruktoria.
        super.init(config);
       try {
       conn = SQL.openConnection();
        } catch (Exception e){
           System.out.println("Poikkeus " + e);
       }
}
    //destroy-metodi soveltuu hyvin tietokantayhteyden sulkemiseen.
    @Override
    public void destroy(){

         try {
          conn.close();
        } catch ( SQLException se ) {
           System.out.println("Poikkeus " + se);
        }
   }

   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       
       PrintWriter out = response.getWriter();

     //Otetaan istunto haltuun
    //HttpSession istunto = request.getSession(false);  
    
    hakusana = request.getParameter("hakusana");
    hakuvalinta = request.getParameter("hakuvalinta");
    
    //Tehdään ArrayList johon voidaan sijoittaa CustomerBean-olioita
    ArrayList<CustomerBean> CustomerList = new ArrayList<>();

     try {
 //luodaan Statement-olio jonka avulla voidaan suorittaa sql-lause
   Statement stmt = conn.createStatement();

   //Suoritetaan sql, otetaan tulosjoukko kiinni.
   //SQL kyselyssä käytetään haettuja hakusana ja hakuvalinta arvoja.
   ResultSet rs = stmt.executeQuery("SELECT * FROM asiakkaat WHERE " + hakuvalinta + " = '" + hakusana + "'");

   //Käydään tulosjoukko eli kaikki asiakkaat läpi
   while ( rs.next() )
   {
     //luetaan tietueen tiedot
    nimi = rs.getString("nimi");
    osoite = rs.getString("osoite");
    puhelin = rs.getString("puhelin");
    email = rs.getString("email");
    salasana = rs.getString("salasana");

    
   // Luodaan papu johon asiakkaan tiedot sijoitetaan
    CustomerBean papu = new CustomerBean();
    //tiedot papuun
     papu.setNimi(nimi);
     papu.setOsoite(osoite);
     papu.setPuhelin(puhelin);
     papu.setEmail(email);
     papu.setSalasana(salasana);
    
    //lisätään jokainen asiakas papuna ArrayListiin
     CustomerList.add(papu);
   
   }
} catch (SQLException e) {
           out.println("Tuli poikkeus!");
       }
     
         request.setAttribute("list", CustomerList);//lähetetään requestin mukana list

        //RequestDispatcher -oliolla voidaan lähettää data jsp-sivulle 
        RequestDispatcher rd = getServletConfig().getServletContext().getRequestDispatcher("/asiakkaat.jsp");
        rd.forward(request, response);

    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }



}

