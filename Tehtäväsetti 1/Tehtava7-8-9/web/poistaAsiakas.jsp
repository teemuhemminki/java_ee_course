﻿<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
    <head>
        <title>Asiakkaan poisto</title>
        <link rel="stylesheet" href="css/tyyli.css" />
    </head>

    <%@ include file="valikko.jsp" %>

    <body>
        <h3>Poista asiakas</h3>

        <%-- Luodaan pudotusvalikko jossa näkyvät kannassa olevat
        s-postiosoitteet. Pudotusvalikosta voidaan valita poistettava.--%>

        <form method="post" action="ServletPoistaAsiakas">

            <table>
                <tr>
                    <td>
                        <select name="email">

                            <c:forEach items="${requestScope.list}" var="item">

                                <option value="${item}"><c:out value="${item}"/></option>

                            </c:forEach>

                        </select>
                    </td>
                    <td>
                        <input type="submit" name="Submit" value="Poista">
                    </td>
                </tr>
            </table> 

        </form>

    </body>
</html>
