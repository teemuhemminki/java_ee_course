/*
 * Noppa.java - Noppa.jsp
 *
 * Tämä on tavallinen Java-luokka, sillä se ei
 * täytä Beanin kriteerejä. 
 *
 * Silti se voidaan ottaa käyttöön action-komennolla jsp:useBean 
 *
 *
 */

package TavallisetLuokat;


import java.util.Random;

public class Noppa {
  
    private int summa = 0;
    private int tulos = 0;
    private int heitot = 0; //Tarvitsi lisätä vain yksi uusi attribuutti. Heittojen lukumäärä.
    private Random rand = new Random();

  
    public int getSumma() {
        return summa; 
    }
  
    public int getTulos() {
        return tulos; 
    }

    public int getHeitot() {
        return heitot;
    }
    
    //Tuotiin getKeskiarvo metodi, joka laskee heittojen keskiarvon ja palauttaa sen.
    public float getKeskiarvo() {
        float keskiarvo = 0;
        if(heitot > 0){ //Tarkistetaan ettei yritetä jakaa nollalla
            keskiarvo = (float) summa / heitot; //Tässä voitaisiin vielä sieventää 2 desimaaliin.
        }
        return keskiarvo;
    }

    public void heita() {
    tulos = rand.nextInt(6)+1;
    summa+=tulos;
    heitot ++; //Heitot nousevat sitä mukaa kun heittoja tehdään.
    }

    public void nollaa() {
    summa = 0;
    heitot = 0; //Heitot nollautuvat summan mukana.
    }
}
