<%-- Noppa.jsp ottaa käyttön olion joka luodaan 
   tavallisesta luokasta. 
   jsp:usebean ei vaadi että luokan tarvitsee olla papu

   Tehtävä: Täydennä sovellusta niin että se ilmoittaa
   heittojen lukumäärän ja nollaa sen. Se myös ilmoittaa
   heittojen keskiarvon ja nollaa sen. Tee sovelluslogiikka
   eli tarvittavat attribuutit ja metodit Noppa -luokkaan.

   Muuta Noppa.jsp:n koodi sellaiseksi että siinä käytetetään
   normaalin JSP:n sijasta mahdollisimman paljon JSTL-
   tagikieltä.

--%>

<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<%--Otetaan jsp:useBean-actionilla Noppa.java käyttöön.
Scope on nyt session, joten Noppa.java on käytettävissä 
tälle sivulle ja kaikille muille sivuille jotka ovat
samassa sessiossa.
--%>
<jsp:useBean id="noppa" scope="session" class="TavallisetLuokat.Noppa" />

<html>
    <head><title>Noppa.jsp</title></head>
    <body>
        <%--
        Tarkastetaan annettu parametri arvo ja sen mukaan toteutetaan toiminto.
        c:choose tagi toimii eräänlaisena switchinä.
        url parametrit haetaan param oliolta.
        Oliot ympäröidään "${olio.metodi}" tyylillä.
        --%>
        <c:choose>
            <c:when test = "${param.heita != null}"> 
                ${noppa.heita()}
                Silmäluvuksi tuli: <c:out value = "${noppa.getTulos()}"/><br>            
            </c:when>
            <c:when test = "${param.nollaa != null}"> 
                ${noppa.nollaa()}
                Noppaa ei ole heitetty<br>
            </c:when>            
            <c:otherwise>
                Noppaa ei ole heitetty<br>
            </c:otherwise>
        </c:choose>
                
        <%-- Tulostetaan loput arvot. <c:out> tagi korvaa <%= tagin --%>
        Heittojen summa on nyt: <c:out value = "${noppa.getSumma()}"/><br>

        Heittojen keskiarvo on nyt: <c:out value = "${noppa.getKeskiarvo()}"/><br>

        Heittoja on nyt: <c:out value = "${noppa.getHeitot()}"/><br>

        <%-- Tuodaan <c:url> tagilla heittolinkkien arvot ja parametrit käyttöön --%>
        <a href= <c:url value="/Noppa.jsp?heita=ok"/> >Heitä noppaa</a><br>
        <a href= <c:url value="/Noppa.jsp?nollaa=ok"/> >Nollaa</a><br>

        <%-- Täytyy myöntää että tiedosto on nyt paljon "puhtaamman" näköinen. --%>

    </body>
</html>