This project is based on following tutorial:
https://www.callicoder.com/spring-boot-mongodb-angular-js-rest-api-tutorial/

Idea is to combine spring boot rest backend with mongodb, angular frontend and use keycloak as authentication.
Projects frontend can be found here: https://bitbucket.org/teemuhemminki/websk2finalassignment/