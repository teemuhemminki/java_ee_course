package fi.hemminki.lopputyo.springkeycloakdemo.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Model for database item.
 * @author Teemu Hemminki
 */

@Document(collection="dbitems")
@JsonIgnoreProperties(value = {"createdAt"}, allowGetters = true)
public class DbItem {
    @Id
    private String id;
    
    @NotBlank
    @Size(max=100)
    @Indexed(unique=true)
    private String title;
    
    private Boolean completed = false;
    
    private Date createdAt = new Date();
    
    private int owner;
    
    public DbItem() {
        super();
    }
    
    @Override
    public String toString() {
        return String.format(
        "DbItem[id='%s', title='%s', completed='%s']", id, title, completed);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public int getOwner() {
        return owner;
    }

    public void setOwner(int owner) {
        this.owner = owner;
    }
    
    
    
}
