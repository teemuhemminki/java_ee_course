package fi.hemminki.lopputyo.springkeycloakdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringkeycloakdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringkeycloakdemoApplication.class, args);
	}
}
