package fi.hemminki.lopputyo.springkeycloakdemo.controllers;

import java.util.List;
import javax.validation.Valid;
import fi.hemminki.lopputyo.springkeycloakdemo.models.DbItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import fi.hemminki.lopputyo.springkeycloakdemo.repositories.SpringKeycloakDemoRepository;

/**
 * Kontrolleri ohjaa tulevia REST kutsuja ja niiden toimintaa.
 * @author Teemu Hemminki
 */
@RestController
@RequestMapping("/api")
@CrossOrigin("*") //Mahdollistaa yhteydenoton apiin toisesta lähteestä
public class DbItemController {

    /*
    Alla otetaan mongo rajapintaan pohjautuva repository yhteys käyttöön.
    Tähän liittyvät metodit ovat suoraan käytettävissä kutsuissa.
    */
    @Autowired
    SpringKeycloakDemoRepository sKDRepository;
    
    @GetMapping("/dbitems")
    public List<DbItem> getAllDbItems(){
        Sort sortByCreatedAtDesc = new Sort(Sort.Direction.DESC, "createdAt");
        return sKDRepository.findAll(sortByCreatedAtDesc);
    }
    
    @PostMapping("/dbitems")
    public DbItem createDbItem(@Valid @RequestBody DbItem dbitem) {
        dbitem.setCompleted(false);
        return sKDRepository.save(dbitem);
    }
    
    @GetMapping(value="/dbitems/{id}")
    public ResponseEntity<DbItem> getDbItemById(@PathVariable("id") String id) {
        return sKDRepository.findById(id)
                .map(dbitem -> ResponseEntity.ok().body(dbitem))
                .orElse(ResponseEntity.notFound().build());
    }
    
    @PutMapping(value="/dbitems/{id}")
    public ResponseEntity<DbItem> updateDbItem(@PathVariable("id") String id, @Valid @RequestBody DbItem dbitem){
        return sKDRepository.findById(id)
                .map(dbItemData -> {
                    dbItemData.setTitle(dbitem.getTitle());
                    dbItemData.setCompleted(dbitem.getCompleted());
                    DbItem updatedDbItem = sKDRepository.save(dbItemData);
                    return ResponseEntity.ok().body(updatedDbItem);
                }).orElse(ResponseEntity.notFound().build());
    }
    
    @DeleteMapping(value="/dbitems/{id}")
    public ResponseEntity<?> deleteDbItem(@PathVariable("id") String id) {
        return sKDRepository.findById(id)
                .map(dbitem -> {
                    sKDRepository.deleteById(id);
                    return ResponseEntity.ok().build();
                }).orElse(ResponseEntity.notFound().build());
    }
}
