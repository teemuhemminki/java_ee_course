package fi.hemminki.lopputyo.springkeycloakdemo.repositories;

import fi.hemminki.lopputyo.springkeycloakdemo.models.DbItem;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Rajapinta, joka laajentaa valmista MongoRepository rajapintaa.
 * Tämä määrittelee automaattisesti kaikki tarvitut mongo CRUD toiminnot
 * ja tuo ne suoraan käyttöön näppärästi.
 * @author Teemu Hemminki
 */

@Repository
public interface SpringKeycloakDemoRepository extends MongoRepository<DbItem, String>{
   
}
