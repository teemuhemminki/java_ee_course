package com.jsf.beans;

import entity.Asiakkaat;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * AsiakasService toimii Asiakkaat entity managerin hakijana ja viejänä
 * 
 * JPA:n hyödyt
 * -Lisää tehokkuutta, sillä JPA on nopea tapa toteuttaa tietokantayhteydet
 * -Lisää koodin uusiokäyttöä, sillä JPA huolehtii itse
 * tietokanta kyselyiden generoinnista käytetyn tietokannan mukaan.
 * -Lisää rakenteellisuutta, sillä JPA:n avulla määritellään tietueen muotoilu olioksi.
 * @author Teemu Hemminki
 */

@Stateless
@LocalBean
public class AsiakasService {
    
    @PersistenceContext
    EntityManager em;
    
    public void saveClient(Asiakkaat asiakas){
        this.em.persist(asiakas);
    }
    
    public List<Asiakkaat> getAllClients(){
        List asiakkaat = this.em.createNamedQuery("Asiakkaat.findAll").getResultList();
        return asiakkaat;
    }    
}
