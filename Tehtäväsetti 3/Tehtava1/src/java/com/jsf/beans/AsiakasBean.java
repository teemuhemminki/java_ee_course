package com.jsf.beans;

import entity.Asiakkaat;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.enterprise.context.RequestScoped;

/**
 * AsiakasBean toimii controllerina joka keskustelee käyttöliittymän
 * ja AsiakasServicen välillä.
 * @author Teemu Hemminki
 */

@ManagedBean(name = "asiakasBean")
@RequestScoped
public class AsiakasBean implements Serializable {

    List<Asiakkaat> clients;
            
    @EJB
    AsiakasService as;
    private Asiakkaat asiakas;
               
    public AsiakasBean() {
        asiakas = new Asiakkaat();
    }
        
    public List<Asiakkaat> getClients(){
        clients = this.as.getAllClients();
        return clients;
    }
     
    public void AddClient(){
        this.as.saveClient(asiakas);
    }


    public void setClients(List<Asiakkaat> clients) {
        this.clients = clients;
    }
    
    public Asiakkaat getAsiakas(){
        return asiakas;
    }
    
    public void setAsiakas(Asiakkaat asiakas){
        asiakas = asiakas;
    }
}
