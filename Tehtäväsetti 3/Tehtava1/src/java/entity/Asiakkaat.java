package entity;

/*
 * Netbeansin generoima luokka Asiakkaat tietokantataulusta. Tästä luodaan Entity-
 * manager-olio, jonka metodeilla (createNamedQuery())voidaan suorittaa alla 
 * luotuja kyselyjä. Niitä voi tietysti myös tehdä itse lisää.
 */


import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "asiakkaat")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Asiakkaat.findAll", query = "SELECT a FROM Asiakkaat a"),
    @NamedQuery(name = "Asiakkaat.findByNimi", query = "SELECT a FROM Asiakkaat a WHERE a.nimi = :nimi"),
    @NamedQuery(name = "Asiakkaat.findByOsoite", query = "SELECT a FROM Asiakkaat a WHERE a.osoite = :osoite"),
    @NamedQuery(name = "Asiakkaat.findByPuhelin", query = "SELECT a FROM Asiakkaat a WHERE a.puhelin = :puhelin"),
    @NamedQuery(name = "Asiakkaat.findByEmail", query = "SELECT a FROM Asiakkaat a WHERE a.email = :email"),
    @NamedQuery(name = "Asiakkaat.findBySalasana", query = "SELECT a FROM Asiakkaat a WHERE a.salasana = :salasana")
})
public class Asiakkaat implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nimi")
    private String nimi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "osoite")
    private String osoite;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "puhelin")
    private String puhelin;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "salasana")
    private String salasana;

    public Asiakkaat() {
    }

    public Asiakkaat(String email) {
        this.email = email;
    }

    public Asiakkaat(String email, String nimi, String osoite, String puhelin, String salasana) {
        this.email = email;
        this.nimi = nimi;
        this.osoite = osoite;
        this.puhelin = puhelin;
        this.salasana = salasana;
    }

    public String getNimi() {
        return nimi;
    }

    public void setNimi(String nimi) {
        this.nimi = nimi;
    }

    public String getOsoite() {
        return osoite;
    }

    public void setOsoite(String osoite) {
        this.osoite = osoite;
    }

    public String getPuhelin() {
        return puhelin;
    }

    public void setPuhelin(String puhelin) {
        this.puhelin = puhelin;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSalasana() {
        return salasana;
    }

    public void setSalasana(String salasana) {
        this.salasana = salasana;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (email != null ? email.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Asiakkaat)) {
            return false;
        }
        Asiakkaat other = (Asiakkaat) object;
        if ((this.email == null && other.email != null) || (this.email != null && !this.email.equals(other.email))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "asiakasrekisteri.Asiakkaat[ email=" + email + " ]";
    }
    
}
