package my.presentation;

import boundary.MessageFacade;
import entities.Message;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 * MessageView papu toimii viewin eli facesin ja messageFacaden välillä
 *
 * @author Teemu Hemminki
 */
@Named(value = "MessageView") //Millä nimellä papua kutsutaan ja käytetään
@RequestScoped //Missä scopessa papu elää ja on luettavissa.
public class MessageView {

    //Liitetään messageFacade enterprise papuun    
    @EJB
    private MessageFacade messageFacade;

    //Luodaan uusi kenttä
    private Message message;

    /**
     * Creates a new instance of MessageView
     */
    public MessageView() {
        this.message = new Message();
    }

    //Palauttaa viestin
    public Message getMessage() {
        return message;
    }

    /* Palauttaa viestien kokonaismäärän
     * käyttäen abstractFacadessa määriteltyä
     * findAll metodia.
     */
    public int getNumberOfMessages() {
        return messageFacade.findAll().size();
    }

    /* Tallentaa viestin ja palauttaa "theend" viestin
     * JSF2.x pyrkii sitten löytämään ja esittämään
     * theend.xml nimisen tiedoston kun postMessagea
     * käytetään.
     */
    public String postMessage() {
        this.messageFacade.create(message);
        return "theend";
    }

}
