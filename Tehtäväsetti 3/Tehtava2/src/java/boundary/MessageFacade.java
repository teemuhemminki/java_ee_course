package boundary;

import entities.Message;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Automaattisesti generoitu papu, joka tekee
 * AbstractFacadesta Message pohjaisen Facaden.
 * 
 * Facade patternin ideana on sitoa monimutkaiset
 * toiminnot yksinkertaiseen käyttöliittymään.
 * @author Teemu Hemminki
 */
@Stateless
public class MessageFacade extends AbstractFacade<Message> {

    @PersistenceContext(unitName = "Tehtava2PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MessageFacade() {
        super(Message.class);
    }
    
}
