/*
Vastauksia tehtävän esittämiin kysymyksiin:

Q: Mitkä komponentit sijaitsevat ejb containerissa ja mitkä web containerissa?
A: ejb paketteihin laitetut komponentit sijaitsevat ejb containerissa,
sillä ne ovat business tierin enterprise papuja ja niiden johdannaisia.
Web containerissa sijaitsevat siis servletit, jotka ovat esillä webissä.

Q: Miten tapahtuu tiedon välitys EJB containerissa ja Web containerissa
sijaitsevien komponenttien välillä?
A: Service joka tarvitsee EJB puolta, injektoi halutun pavun @EJB
annotiaatiolla tai halutun factoryn @Resource annotaatiolla.

Q:Mitä hyötyä tästä arkkitehtuuriratkaisusta voi olla?
A: EJB mallilla pystytään eristämään business logiikka clientin ja frontin
puolelta pois. Näin kehitystyötä voidaan tehdä helpommin eristetysti ja 
ohjelman toiminta on paremmin turvassa ja clientin tarvitsee käyttää vähemmän
omia resurssejaan.


Q:Millaisissa sovelluksissa ja miksi JMS:ää tai vastaavaa
viestijonopalvelua käytetään?
A: Viestijonopalveluiden tavoite on ehkäistä "Tuottaja-Kuluttaja" ongelma, jossa
kuluttaja ei saa yrittää lisätä sisältöä täynnä olevaan jonoon ja kuluttajan ei
tulisi ottaa sisältöä tyhjästä jonosta.
Viestijonopalvelut toimivat hyvin silloin, jos viestejä pitää toimittaa
asynkronisesti. Esimerkiksi tilauksen tai laskutuksen käsittelyssä.
*/

package ejb;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * NewsEntity toimii tietomallina annettavalle tiedolle.
 * Tästä entiteetistä luodaan facade jota käytetään tietokannan
 * yhteydessä.
 * @author Teemu Hemminki
 */
@Entity
public class NewsEntity implements Serializable {

    private String title;
    private String body;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
    
    
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NewsEntity)) {
            return false;
        }
        NewsEntity other = (NewsEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ejb.NewsEntity[ id=" + id + " ]";
    }
    
}
